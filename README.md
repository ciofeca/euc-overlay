## Description

Overlaying a HUD-like (head-up display) set of indicator gauges built from [EUC World](https://euc.world) log file data to the matching video stream. (EUC stands for "electric unicycle").

HUD includes a speed indicator (numeric and arc-styled), a power indicator (bar-styled), battery level (percent) and trip distance (numeric). Other data, even if collected (location, voltage, current) may or may not be included in future releases.

There is some preliminary support for GoPro GPS location/speed metadata (MET track). Note that GPS data is not always available and may contain interpolated values. For example, while GoPro Hero 8 emits about 10 GPS records per second, it's actually one per second followed by interpolated values. The wheel speed indicator from EUC World is better (actual rotations vs time) and with reasonable granularity (about 6 records per second on an Inmotion V11).

Note: the current version still requires the EUC World log file to be *manually trimmed* to the exact start/end times of the video.

Input: configuration file, clipped video file, EUC CSV log file (edited to make it in sync with the video clip).

Output: video clip with added HUD.


## Requirements

Assuming a command-line environment (bash shell) and a filesystem capable of soft/hard links (any Linux is OK).

Requires:

- Ruby 3.x (probably 2.7 still OK)

- ImageMagick 6.9 (probably older 6.x still OK; only the *convert* executable is required)

- ffmpeg 4.4 (probably older 4.x still OK, as long as codecs are there, including the *qtrle* codecs)

- [EUC World](https://euc.world) log files related to the video recordings.

Works with MP4 video files, full-HD videos (1920×1080) and 2.7k (2704×1520), tested with GoPro Hero 8 video clips.

Current EUC World app only runs on Android cellphones (and full Android smartwatches). Logging function has to be enabled before the trip (Settings / DataLogging / StartLoggingAutomatically). Its *EUC...timestamp.csv* log files are saved in the *Downloads/EUC World* directory of the Android device.


## Limits

Currently supporting full-HD or 2.7k, up to 60 fps; other resolutions will need some hacking in the *convert* parameters.

HUD intermediate video files need a codec supporting alpha-channel encoding; *qtrle* is OK; *prores* is slow.

The EUC World CSV file has to be edited to match the video clip initial and final timestamp (wipe out top records that happen before the first video frame, wipe out the bottom records that do not appear in the video).


## Workflow (how does this work?)

- builds a set of PNG files with transparent background (will be used as overlay frames)

- extracts speed, power and timestamps from EUC World log files and builds a script with framenumbers

- creates a few transparent ("alpha") overlay video tracks to be mixed with the original video clip; every track is built out of a sequence of transparent frames (hard links to original PNG files)

- combines the video streams and reencodes at the configured bitrate

- also builds a low resolution version (1280 pixels wide) of the output video.


## Examples

Normal usage:

    # 1. record video while euc.world is logging data
    # 2. extract relevant MP4 video clip and related csv log
    # 3. edit csv log, erasing top and bottom lines not related to the video
    # 4. edit configuration file

    # 5. build output video;
    ./euc-overlay build GH010234.MP4 edited_eucworld_log.csv outputvideo.mp4

    # 6. upload outputvideo.mp4 to youtube/rumble/etc

How to check resolution/fps:

    ./res GH010234.MP4

How to redo some step:

    ./euc-overlay hud GH010234.MP4 edited_eucworld_log.csv outputvideo.mp4

Data extraction for debugging:

    ./extract-gopro-metadata GH010234.MP4 > tmp234.txt
    ./extract-eucworld-log EUC*data*204259.csv > euc.txt


## FAQs

*EUC World does it better than that.* Yes, but using the cellphone camera; does not support external action cameras. Also, currently it's a subscribers-only feature.

*Insta360/GoPro apps do it better than that.* Yes, but only relying on GPS data; possibly a paid feature.

*Some action cams save GPS info in the video track; why don't you mix in that data?* Wheel rotation gives an exact speed value; GPS gives an estimated speed value (based on satellites in view and signal measurements, without taking into account the actual wheel rotation). Also, EUC World log files usually have better granularity than one-read-per-second-plus-interpolation of most GPS devices. However, GPS info may add location, elevation, direction.

*Where is the gauge template?* The gauge components are built with the *convert* commands (outputting PNG files); font and (some) colors are defined in the *configuration* file.

*Why the "somewhat red with a bit of white shadow"?* Based on my recorded clips library, different color schemes or multiple colors style felt a bit distracting.


## Example output (still frame)

![example frame](./view-example.jpg)
