#!/usr/bin/env ruby

# output main attributes of a video file
# uses "ffmpeg" to extract them


# will complain if not an approved resolution:
#
RESOLUTIONS=[ 4000, 3840, 2704, 2560, 1920, 1280, 1080, 854, 848, 720, 640, 432 ]

ARGS="-res -fps -kbit -rot -gpmd".split

if [ '-h', '-v', '--help', nil ].include? ARGV.first
  STDERR.puts "!--usage: #$0 [ #{ARGS.join ' | '} ] videofilename..."
  exit 1
end

totalsize, filesize = 1
request = if ARGS.include? ARGV.first
            ARGV.delete_at 0
          else
            ''
          end

ARGV.each do |videofile|
  fp = IO.popen "ffmpeg -i #{videofile.inspect} 2>&1"
  xsize, ysize, fps, kbit, met, rot, dur = nil

  while a = fp.gets
    if a.strip.start_with? 'Duration:'
      dur = a.strip.split[1].chop
    end

    break  if a.strip.start_with? 'Stream #0:0'
  end
  fail "ffmpeg cannot access #{videofile.inspect}"  unless a

  a.split(', ').each do |b|
    if b =~ /^\d+x\d+/         # first one appearing is the resolution ("pixels x pixels")
      unless xsize || ysize
        xsize, ysize = $&.strip.split('x')
        if request == ARGS.first  # -res
          puts "#{xsize}x#{ysize}"
          exit
        end
      end
    end

    if b =~ /\d+(\.\d+)* fps/  # integer or floating point "frames per second" value
      fps = $&.split.first
      fail "unexpected fps value (#{b})"  if fps.to_f < 20.5 || fps.to_f > 480.0
      if request == ARGS[1]  # -fps
        puts fps
        exit
      end
    end

    if b =~ /\d+(\.\d+)* kb\/s/  # integer or floating point "kilobits per second" value
      kbit = $&.split.first
      fail "unexpected kbit value (#{b})"  if kbit.to_f < 64.0 || kbit.to_f > 555000.0
      if request == ARGS[2]  # -kbit
        puts kbit
        exit
      end
    end
  end

  while a = fp.gets
    if a =~ /displaymatrix/  # rotation in degrees, usually 0 or -180
      rot = $'.split[3]
      if request == ARGS[3]  # -rot
        puts rot
        exit
      end

      rot += '°'
    end

    if a.strip.start_with? 'Stream #0:'
      if a =~ /gpmd/  # gopro GPS metadata track is present
        met = a.split('Stream #0:')[1][0..0]
        if request == ARGS[4]  # -gpmd
          puts met
          exit
        end

        break  # because no more interesting info coming
      end
    end
  end

  if request == ARGS[4]  # -gpmd
    puts 'noGPS'
    exit
  end

  fail "cannot get resolution/fps from #{videofile.inspect} --- #{xsize.inspect} #{ysize.inspect} #{fps.inspect}"  unless (xsize && ysize && fps)
  fail "unknown resolution #{xsize}x#{ysize}"  unless RESOLUTIONS.include? xsize.to_i

  filesize = File.size videofile
  totalsize += filesize
  fsize = filesize / 1_000_000  # decimal base, so that the file looks bigger (teehee)

  puts "%s\t%4d Mb   %sx%s   %8s   %6s   %s   %s   %s" % [ videofile, fsize, xsize, ysize, fps, kbit, rot, dur, met ? "+GPS" : '' ]
end

printf("\t\t%4d Mb\n", totalsize / 1_000_000)  unless totalsize == filesize

# ---
